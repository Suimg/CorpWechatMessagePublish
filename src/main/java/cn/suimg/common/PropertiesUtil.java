package cn.suimg.common;

import cn.suimg.common.exception.LoadApplicationException;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {


    private static final Properties properties = new Properties(){{
        try {
            load(new ClassPathResource("common.properties").getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }};

    public static String getValue(String key) {
        return properties.getProperty(key.trim());
    }

    public static <T> T getValue(String key, Class<T> clazz) {
        return covert(getValue(key), clazz);
    }

    @SuppressWarnings("unchecked")
    private static <T> T covert(String value, Class<T> clazz) {
        //1.优先转换方案(switch不支持class类型没办法。)
        if (String.class.equals(clazz)) {
            return (T) value;
        } else if (Integer.class.equals(clazz)) {
            return (T) Integer.valueOf(value);
        } else if (Boolean.class.equals(clazz)) {
            return (T) Boolean.valueOf(value);
        } else if (Double.class.equals(clazz)) {
            return (T) Double.valueOf(value);
        } else if (Float.class.equals(clazz)) {
            return (T) Float.valueOf(value);
        } else if (Long.class.equals(clazz)) {
            return (T) Long.valueOf(value);
        } else if (Byte.class.equals(clazz)) {
            return (T) Byte.valueOf(value);
        } else if (Short.class.equals(clazz)) {
            return (T) Short.valueOf(value);
        }

        return JSONUtil.toObject(value, clazz);
    }
}
