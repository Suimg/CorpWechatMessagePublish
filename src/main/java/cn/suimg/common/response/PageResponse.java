package cn.suimg.common.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * bootstrap table 分页要求的结果
 * @param <T>
 */
@Data
public class PageResponse {

    /**
     * 一共有多少条记录
     */
    private Integer total;

    /**
     * 当前列表
     */
    private List<Object> rows;


    public PageResponse() {
        this.rows = new ArrayList<>();
    }

    /**
     * 添加一项到列表
     * @param data
     * @return
     */
    public PageResponse add(Object data){
        this.rows.add(data);
        return this;
    }

    /**
     * 添加所有结果到列表
     * @param data
     * @return
     */
    public PageResponse addAll(List<? extends Object> data){
        this.rows.addAll(data);
        return this;
    }

    /**
     * 设置总共记录(非rows.size)
     * @param total
     * @return
     */
    public PageResponse setTotal(Integer total){
        this.total = total;
        return this;
    }

    /**
     * 标准的成功响应结果
     * @param total
     * @param rows
     * @param <T>
     * @return
     */
    public static PageResponse success(Integer total,List<? extends Object> rows){
        return new PageResponse().setTotal(total).addAll(rows);
    }

    public static PageResponse success(){
        return new PageResponse().setTotal(0);
    }
}
