package cn.suimg.common.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

@Data
public class ServerResponse implements Serializable {

    private int retcode;

    private String message;

    private Object databody;

    public ServerResponse() {

    }

    public ServerResponse(int retcode, String message) {
        this.retcode = retcode;
        this.message = message;
    }

    public ServerResponse(int retcode, String message, Object databody) {
        this.retcode = retcode;
        this.message = message;
        this.databody = databody;
    }

    public static ServerResponse success() {
        return new ServerResponse(StatusCode.SUCCESS.getRetcode(), StatusCode.SUCCESS.getMessage());
    }

    public static ServerResponse success(String message) {
        return success(message, null);
    }

    public static ServerResponse success(String message, Object data) {
        return new ServerResponse(StatusCode.SUCCESS.getRetcode(), message, data);
    }

    public static ServerResponse failed() {
        return new ServerResponse(StatusCode.FAILD.getRetcode(), StatusCode.FAILD.getMessage());
    }

    public static ServerResponse failed(String message) {
        return new ServerResponse(StatusCode.FAILD.getRetcode(), message);
    }

    public static ServerResponse error() {
        return new ServerResponse(StatusCode.ERROR.getRetcode(), StatusCode.ERROR.getMessage());
    }

    public static ServerResponse error(String message) {
        return new ServerResponse(StatusCode.ERROR.getRetcode(), message);
    }

    @JSONField(serialize = false)
    public boolean isSuccess() {
        return retcode == 0;
    }
}