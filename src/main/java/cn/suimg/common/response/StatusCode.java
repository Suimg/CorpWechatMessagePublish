package cn.suimg.common.response;

import lombok.Data;

/**
 * 返回状态码
 */
@Data
public class StatusCode {

    /**
     * 请求成功执行
     */
    public static final StatusCode SUCCESS = new StatusCode(0, "请求成功执行");

    /**
     * 请求未能成功执行
     */
    public static final StatusCode FAILD = new StatusCode(1, "请求未能成功执行");

    /**
     * 服务器异常
     */
    public static final StatusCode ERROR = new StatusCode(-1, "服务器异常");

    /**
     * 响应状态码
     */
    private Integer retcode;

    /**
     * 响应信息
     */
    private String message;

    /**
     * StatusCode
     *
     * @param retcode
     * @param message
     */
    private StatusCode(Integer retcode, String message) {
        this.retcode = retcode;
        this.message = message;
    }
}
