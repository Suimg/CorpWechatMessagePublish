package cn.suimg.common;

/**
 * 公用常量类
 * @author Suimg
 */
public interface Constant {

    /**
     * 查询企业微信的访问密钥地址
     */
    String CORP_WECHAT_GET_ACCESS_TOKEN_URL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s";

    /**
     * 企业微信推送消息接口地址
     */
    String CORP_WECHAT_SEND_MESSAGE_URL = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s";

    /**
     * 获取企业微信应用信息接口
     */
    String CORP_WECHAT_GET_AGENT_INFO_URL = "https://qyapi.weixin.qq.com/cgi-bin/agent/get?access_token=%s&agentid=%s";

    /**
     * 获取部门列表接口
     */
    String CORP_WECHAT_GET_DEPT_INFO_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=%s";

    /**
     * 上传媒体文件接口
     */
    String CORP_WECHAT_UPLOAD_MEDIA_URL = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";
}
