package cn.suimg.wechat.enmus;

/**
 * 媒体文件类型
 * @author Suimg
 */
public enum MediaFileType {


    /**
     * 图片
     */
    Image("image"),

    /**
     * 语音
     */
    Voice("voice"),

    /**
     * 视频
     */
    Video("video"),

    /**
     * 文件
     */
    File("file")

    ;

    /**
     * 文件类型
     */
    private final String type;

    MediaFileType(String type){
        this.type = type;
    }


    /**
     * 解析枚举
     * @param type
     * @return
     */
    public static MediaFileType parse(String type){
        for(MediaFileType messageType:values()){
            if(messageType.type.equals(type))
                return messageType;
        }
        return null;
    }

    @Override
    public String toString() {
        return type;
    }
}
