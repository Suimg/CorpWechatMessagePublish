package cn.suimg.wechat.enmus;

/**
 * 推送消息类型
 * @author Suimg
 */
public enum PublishMessageType {

    /**
     * 文本消息
     */
    TextMessage("text"),


    /**
     * 图片消息
     */
    ImageMessage("image"),

    /**
     * 语音消息
     */
    VoiceMessage("voice"),

    /**
     * 视频消息
     */
    VideoMessage("video"),

    /**
     * 文件消息
     */
    FileMessage("file"),

    /**
     * 文本卡片
     */
    TextCardMessage("textcard"),

    /**
     * 新闻消息(图文消息)
     */
    NewsMessage("news"),

    /**
     * Markdown格式的消息
     */
    MarkdownMessage("markdown"),

    ;

    /**
     * 消息类型
     */
    private final String type;


    PublishMessageType(String type){
        this.type = type;
    }

    /**
     * 解析枚举
     * @param type
     * @return
     */
    public static PublishMessageType parse(String type){
        for(PublishMessageType messageType:values()){
            if(messageType.type.equals(type))
                return messageType;
        }
        return null;
    }

    @Override
    public String toString() {
        return type;
    }
}
