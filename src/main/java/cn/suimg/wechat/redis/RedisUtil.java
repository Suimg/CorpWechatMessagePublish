package cn.suimg.wechat.redis;

import cn.suimg.wechat.util.SpringContextUtils;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * Redis工具类
 **/
public class RedisUtil {

    /**
     * Redis 对象
     */
    public static StringRedisTemplate redisTemplate = SpringContextUtils.getBean(StringRedisTemplate.class);

    /**
     * 录入String值同时设定过期时间
     *
     * @param key
     * @param seconds
     * @param value
     */

    public static void set(String key, String value, int seconds) {
        redisTemplate.opsForValue().set(key, value, seconds, TimeUnit.SECONDS);
    }

    /**
     * 录入String值
     *
     * @param key
     * @param value
     */
    public static void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 根据key获取字符串数据
     *
     * @param key
     * @return
     */
    public static String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 根据key删除内容
     *
     * @param key
     */
    public static void delete(String key) {
        redisTemplate.delete(key);
    }
}
