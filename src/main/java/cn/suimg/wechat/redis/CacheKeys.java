package cn.suimg.wechat.redis;


/**
 * Cache Key
 * 存储Redis Key
 */
public class CacheKeys {

    /**
     * 企业微信访问密钥缓存
     */
    public static final CacheKeys CORP_WECHAT_ACCESS_TOKEN = new CacheKeys("corp_wechat_access_token:", 15);
    public static final CacheKeys CORP_WECHAT_PUBLISH_KEY_CACHE = new CacheKeys("corp_wechat_publish_key_cache:", 1);

    private String key;

    private int db;

    private CacheKeys(String key, int db) {
        this.key = key;
        this.db = db;
    }

    public static CacheKeys keyLink(CacheKeys cacheKeys, Object linkStr) {
        return new CacheKeys(cacheKeys.getKey() + linkStr.toString() , cacheKeys.getDb());
    }

    String getKey() {
        return key;
    }

    int getDb() {
        return db;
    }
}
