package cn.suimg.wechat.service.impl;

import cn.suimg.common.CommonUtil;
import cn.suimg.common.EncryptUtil;
import cn.suimg.common.JSONUtil;
import cn.suimg.wechat.bean.CorpWechat;
import cn.suimg.wechat.bean.CorpWechatExample;
import cn.suimg.wechat.dao.CorpWechatMapper;
import cn.suimg.wechat.redis.CacheKeys;
import cn.suimg.wechat.redis.CacheOperator;
import cn.suimg.wechat.service.CorpWechatService;
import cn.suimg.wechat.util.CorpWechatUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;


/**
 * 企业微信服务类实现
 * @author Suimg
 */
@Service
public class CorpWechatServiceImpl implements CorpWechatService {

    @Resource
    private CorpWechatMapper corpWechatMapper;

    @Resource
    private CacheOperator cacheOperator;

    /**
     * 保存企业微信信息
     *
     * @param corpWechat
     * @return
     */
    @Override
    public String saveCorpWechatInfo(CorpWechat corpWechat) {
        String id = CommonUtil.randomUUID();
        corpWechat.setId(id);

        CorpWechatUtil corpWechatInstance = CorpWechatUtil.getInstance(corpWechat.getCorpId(), corpWechat.getCorpSecret(), corpWechat.getAgentId());

        //自动填充企业名称
        corpWechat.setCorpName(corpWechatInstance.getCorpName());

        //获取并自动填入应用信息
        CorpWechat dto = corpWechatInstance.getAgentInfo();
        corpWechat.setAgentName(dto.getAgentName());
        corpWechat.setAgentAvatarUrl(dto.getAgentAvatarUrl());

        //推送的Key做的长一点(64位)防止撞库瞎推消息
        String publishKey = EncryptUtil.md5(String.format("%s=>%d=>%s", id, System.currentTimeMillis(), CommonUtil.randomUUID())) + EncryptUtil.md5(JSONUtil.toString(corpWechat));
        corpWechat.setPublishKey(publishKey);

        //写入缓存再写入数据库做持久化
        cacheOperator.set(CacheKeys.keyLink(CacheKeys.CORP_WECHAT_PUBLISH_KEY_CACHE,corpWechat.getPublishKey()),corpWechat);
        corpWechatMapper.insert(corpWechat);
        return id;
    }

    /**
     * 更新企业微信信息
     *
     * @param corpWechat
     */
    @Override
    public void updateCorpWechatInfo(CorpWechat corpWechat) {

    }

    /**
     * 查询当前用户下企业微信列表
     *
     * @param userId
     * @return
     */
    @Override
    public List<CorpWechat> queryCorpWechatList(String userId) {
        return null;
    }

    /**
     * 删除企业微信信息
     *
     * @param id
     * @param userId
     */
    @Override
    public void removeCorpWechat(String id, String userId) {

    }

    /**
     * 通过推送的key查询微信对象
     *
     * @param publishKey
     * @return
     */
    @Override
    public CorpWechat getCorpWechatByPublishKey(String publishKey) {
        CorpWechat corpWechat;
        CacheKeys cacheKey = CacheKeys.keyLink(CacheKeys.CORP_WECHAT_PUBLISH_KEY_CACHE, publishKey);
        if((corpWechat = cacheOperator.get(cacheKey,CorpWechat.class)) == null){
            corpWechat = CommonUtil.getListItem(corpWechatMapper.selectByExample(new CorpWechatExample(){{
                createCriteria().andPublishKeyEqualTo(publishKey);
            }}),0);
            if(corpWechat != null)
                cacheOperator.set(cacheKey,corpWechat);
        }
        return corpWechat;

    }
}
