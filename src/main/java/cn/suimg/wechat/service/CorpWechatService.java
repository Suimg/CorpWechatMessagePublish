package cn.suimg.wechat.service;

import cn.suimg.wechat.bean.CorpWechat;

import java.util.List;

/**
 * 企业微信服务类
 * @author Suimg
 */
public interface CorpWechatService {

    /**
     * 保存企业微信信息
     * @param corpWechat
     * @return
     */
    String saveCorpWechatInfo(CorpWechat corpWechat);


    /**
     * 更新企业微信信息
     * @param corpWechat
     */
    void updateCorpWechatInfo(CorpWechat corpWechat);

    /**
     * 查询当前用户下企业微信列表
     * @param userId
     * @return
     */
    List<CorpWechat> queryCorpWechatList(String userId);


    /**
     * 删除企业微信信息
     * @param id
     * @param userId
     */
    void removeCorpWechat(String id,String userId);


    /**
     * 通过推送的key查询微信对象
     * @param publishKey
     * @return
     */
    CorpWechat getCorpWechatByPublishKey(String publishKey);


}
