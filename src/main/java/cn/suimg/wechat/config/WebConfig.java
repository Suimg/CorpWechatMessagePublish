package cn.suimg.wechat.config;

import cn.suimg.common.JSONUtil;
import cn.suimg.common.filter.CrossDomainFilter;
import cn.suimg.common.filter.IndexFilter;
import cn.suimg.common.interceptor.FirewallInterceptor;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.servlet.ServletRequestListener;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Web Config
 *
 * @author suimg
 *
 */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(WebConfig.class);


    /**
     * 过滤静态资源
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/favicon.ico").addResourceLocations("classpath:/favicon.ico");
        registry.addResourceHandler("/robots.txt").addResourceLocations("classpath:/robots.txt");
    }

    @Bean
    public ServletListenerRegistrationBean<ServletRequestListener> getServletListenerRegistrationBean(){
        return new ServletListenerRegistrationBean<>(new RequestContextListener());
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters){
        converters.add(new FastJsonHttpMessageConverter(){{
            setFastJsonConfig(new FastJsonConfig(){{
                setSerializerFeatures(JSONUtil.filters);
                setSupportedMediaTypes(Arrays.asList(
                        MediaType.ALL,
                        MediaType.APPLICATION_JSON
                ));
            }});
        }});
    }

    /**
     * 配置跨域过滤器
     * @return
     */
    @Bean
    public FilterRegistrationBean indexFilterRegistration() {
        return new FilterRegistrationBean(){{
            setFilter(new IndexFilter());
            addUrlPatterns("/*");
            setName("indexFilter");
        }};
    }

    /**
     * 配置跨域过滤器
     * @return
     */
    @Bean
    public FilterRegistrationBean<CrossDomainFilter> crossDomainFilterRegistration() {
       return new FilterRegistrationBean(){{
           setFilter(new CrossDomainFilter());
           addUrlPatterns("/**");
           setName("crossDomainFilter");
       }};
    }

    /**
     * Auth Interceptor
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new FirewallInterceptor()).addPathPatterns("/**");
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DruidDataSource druidDataSource(){
        return new DruidDataSource();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.redis.config")
    public JedisPoolConfig getJedisPoolConfig(){
        return new JedisPoolConfig();
    }

    @Bean
    public JedisPool getJedisPool(JedisPoolConfig config,@Value("${spring.redis.host}") String host,@Value("${spring.redis.port}") Integer port,@Value("${spring.redis.password}") String password){
        if(StringUtils.isBlank(password))
            return new JedisPool(config,host,port);
        return new JedisPool(config,host,port,60,password);
    }

}