package cn.suimg.wechat.controller;

import cn.suimg.common.request.BasicController;
import cn.suimg.common.response.ServerResponse;
import lombok.SneakyThrows;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author Suimg
 */
@Controller
@RequestMapping("/error")
public class ExceptionController extends BasicController implements ErrorController{

    @Override
    public String getErrorPath() {
        return "error";
    }

    @RequestMapping
    @ResponseBody
    public ServerResponse index(){
        HttpServletResponse response = getResponse();
        return ServerResponse.error("404 not found : " + getRequest().getRequestURI());
    }



}
