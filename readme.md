# 企业微信消息推送大明白说明书

> 现在已更新消息推送接口，支持b64认证 。消息推送支持 文本推送和markdown推送

### 编译依赖环境

* JDK1.8+

* Maven

### 其他服务依赖

* redis
> apt-get install redis-server/yum install redis-server 即可安装
* <del>suimg-common 目前该项目还没开源，只能放出一个jar包再安装到本地 或者执行install-suimg-common.bat (依赖于本地Maven和JDK的正确配置)</del>
* suimg-common已经集成到项目当中

### 修改配置文件

```yaml
spring:
    application:
        name: suimg-wechat
    datasource:
        tomcat:
        	initialSize: 20
        	max-active: 100
        	max-idle: 50
        	initSQL: SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci
        type: com.alibaba.druid.pool.DruidDataSource
        #修改数据库地址和用户名密码
        url: jdbc:mysql://127.0.0.1/suimg?useSSL=false&characterEncoding=UTF-8&serverTimezone=UTC
        username: root
        password: root
    redis:
    	#Redis的配置
        host: 127.0.0.1
        port: 6379
        config:
            testOnBorrow: false
            maxIdle: 200
            maxTotal: 100
            maxWaitMills: 1000
        #redis的密码 如果没有就留空    
        password:             
```

### 数据库信息
* 新建一个数据库 名称 suimg 编码 utf8-mb4
```sql
CREATE TABLE `corp_wechat` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `corp_id` varchar(64) DEFAULT NULL COMMENT '微信企业ID',
  `corp_name` varchar(255) DEFAULT NULL COMMENT '企业名称',
  `corp_secret` varchar(64) DEFAULT NULL COMMENT '微信企业密钥',
  `agent_id` varchar(64) DEFAULT NULL COMMENT '应用ID',
  `agent_name` varchar(255) DEFAULT NULL COMMENT '应用名称',
  `agent_avatar_url` varchar(255) DEFAULT NULL COMMENT '应用头像地址',
  `publish_key` varchar(255) DEFAULT NULL COMMENT '推送消息的Key',
  `user_id` varchar(32) DEFAULT NULL COMMENT '关联的用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```





